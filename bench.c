/*
 * Copyright © 2018 Benedikt Terhechte and Bart Massey
 * [This program is licensed under the "MIT License"]
 * Please see the file LICENSE in the source
 * distribution of this software for license terms.
 */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

static inline uint64_t*
resize_image(uint64_t* image, uint64_t size,
             uint64_t width, uint64_t scale, uint64_t* rsize) {
    uint64_t *result = malloc(sizeof(uint64_t) * size / scale);
    assert(result);
    uint64_t pos = 0;
    for (uint64_t i = 0; i < size; i += width) {
        for (uint64_t i2 = i; i2 < i + width; i2 += scale) {
            uint64_t sum = 0;
            for (uint64_t i3 = i2; i3 < i2 + scale; i3++) {
                sum += image[i3];
            }
            result[pos++] = sum / scale;
        }
    }
    *rsize = pos;
    return result;
}

static uint64_t* generate(uint64_t* size) {
    const uint64_t count = 2000000;
    uint64_t image[] = {
        1, 0, 0, 4, 4, 0, 0, 1,
        0, 0, 0, 9, 9, 0, 0, 0,
        0, 0, 0, 9, 9, 0, 0, 0,
        4, 9, 9, 9, 9, 9, 9, 4,
        4, 9, 9, 9, 9, 9, 9, 4,
        0, 0, 0, 9, 9, 0, 0, 0,
        0, 0, 0, 9, 9, 0, 0, 0,
        1, 0, 0, 4, 4, 0, 0, 1,
    };
    uint64_t* array = malloc(count * sizeof(image));
    assert(array);
    uint64_t pos = 0;
    for (uint64_t i = 0; i<count; i++) {
        for (uint64_t j = 0; j < sizeof(image) / sizeof(uint64_t); j++) {
            array[pos++] = image[j];
        }
    }
    *size = pos;
    return array;
}

static uint64_t csum(uint64_t *thingy, uint64_t thingy_size) {
    uint64_t s = 0;
    for (uint64_t i = 0; i < thingy_size; i++)
        s += thingy[i];
    return s;
}

int main(int argc, const char * argv[]) {
    uint64_t image_size = 0;
    uint64_t* image = generate(&image_size);

    uint64_t result1_size = 0, result2_size = 0, result3_size = 0;
    uint64_t *result1 = resize_image(image, image_size, 8, 2, &result1_size);
    uint64_t *result2 = resize_image(image, image_size, 32, 8, &result2_size);
    uint64_t *result3 = resize_image(image, image_size, 16, 4, &result3_size);

    printf("%lu %lu %lu\n",
           csum(result1, result1_size),
           csum(result2, result2_size),
           csum(result3, result3_size));
    free(result1);
    free(result2);
    free(result3);
    free(image);
    return 0;
}
