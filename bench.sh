#!/bin/sh
# Copyright © 2018 Benedikt Terhechte and Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file LICENSE in the source
# distribution of this software for license terms.

TIME_FORMAT='  real %e\n  user %U\n  sys %S\n'

make
for b in bench-swift bench-rs bench-c bench-c-gcc
do
    case $b in
        bench-swift) VERSION="swiftc `swiftc --version | sed -e '2,$d' -e 's/.*version //' -e 's/ .*//'`" ;;
        bench-rs) VERSION="rustc `rustc --version | sed -e '2,$d' -e 's/.*rustc //' -e 's/ .*//'`" ;;
        bench-c)  VERSION="clang `clang --version | sed -e '2,$d' -e 's/.*version //' -e 's/ .*//'`" ;;
        bench-c-gcc) VERSION="gcc `gcc --version | sed -e '2,$d' -e 's/.* //'`" ;;
        *) echo "warning: unknown benchmark $b" >&2; VERSION="$b" ;;
    esac
    echo $b "($VERSION):"
    /usr/bin/time -f "$TIME_FORMAT" ./$b | sed 1d
done
