/*
 * Copyright © 2018 http://reddit.com/u/terhecte and Bart Massey
 * [This program is licensed under the "MIT License"]
 * Please see the file LICENSE in the source
 * distribution of this software for license terms.
 */

func resize(image: [Int], width: Int, scale: Int) -> [Int] {
    var result = [Int]()
    result.reserveCapacity(image.count / scale)
    for i in stride(from: 0, to: image.count, by: width) {
        for i2 in stride(from: i, to: (i + width), by: scale) {
            var sum = 0
            for i3 in i2..<(i2 + scale) {
                sum += image[i3]
            }
            result.append(sum / scale)
       }
    }
    return result
}

func generate() -> [Int] {
    let image = [
        1, 0, 0, 4, 4, 0, 0, 1,
        0, 0, 0, 9, 9, 0, 0, 0,
        0, 0, 0, 9, 9, 0, 0, 0,
        4, 9, 9, 9, 9, 9, 9, 4,
        4, 9, 9, 9, 9, 9, 9, 4,
        0, 0, 0, 9, 9, 0, 0, 0,
        0, 0, 0, 9, 9, 0, 0, 0,
        1, 0, 0, 4, 4, 0, 0, 1
        ]
    let nr = 2_000_000
    var result = [Int]()
    result.reserveCapacity(nr * image.count)
    for _ in 0..<nr {
        result.append(contentsOf: image)
    }
    return result
}

func csum(thingy: [Int]) -> Int {
    var s = 0
    for i in 0..<thingy.count {
        s += thingy[i]
    }
    return s
}

func main()  {
    let image = generate()
    let result1 = csum(thingy: resize(image: image, width: 8, scale: 2))
    let result2 = csum(thingy: resize(image: image, width: 32, scale: 8))
    let result3 = csum(thingy: resize(image: image, width: 16, scale: 4))
    print(result1, result2, result3)
}

main()
