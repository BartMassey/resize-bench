# Resize-bench
Copyright (c) 2018 Benedikt Terhechte and Bart Massey

Code from
http://www.reddit.com/r/rust/comments/9nnzeo/why_could_this_rust_code_be_slow/
by Benedikt Terhechte (Twitter @terhechte) that benchmarks a
simple image rescaling algorithm in C, Swift, and Rust.

I corrected the C code, cleaned up the C and Rust and made
them as corresponding as I could. The code checksums the
images at the end to ensure that they are used, and to
ensure that all versions are producing the same answer. I
increased the image size by a factor of 10 to improve timing
precision. There are various branches corresponding to
speedup experiments suggested on Reddit.

The answer to the original Rust performance question turns
out to be putting `#[inline(always)]` at the top of the Rust
`resize_image` function. Apparently Rust's inlining
heuristics don't inline `resize_image`, which may actually
be reasonable.  Once it is inlined, constant propagation
does its magic. Oddly, making `resize_image` be `inline
static` in the C version doesn't fix the GCC performance
issue: the GCC code remains twice as slow as the other
versions.

Comically, the program text size of the Rust executable is
100x larger than the other executables. Somebody should
really work on this someday.

Times on my Debian Linux box (Haswell desktop, plenty of
memory). No "native" flags, which produce a noticeable
speedup on all versions.

```
bench-swift (swiftc 4.2):
  real 0.94
  user 0.53
  sys 0.39

bench-rs (rustc 1.29.1):
  real 0.92
  user 0.55
  sys 0.37

bench-c (clang 4.0.1-10+b1):
  real 0.80
  user 0.37
  sys 0.43

bench-c-gcc (gcc 8.2.0):
  real 1.60
  user 1.19
  sys 0.40
```

-----

This program is licensed under the "MIT License".
Please see the file `LICENSE` in the source
distribution of this software for license terms.

