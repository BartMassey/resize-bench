// Copyright © 2018 Benedikt Terhechte and Bart Massey
// [This program is licensed under the "MIT License"]
// Please see the file LICENSE in the source
// distribution of this software for license terms.

#[inline(always)]
fn resize_image(image: &[usize], width: usize, scale: usize) -> Vec<usize> {
    let mut result: Vec<usize> = Vec::with_capacity(image.len());
    for i in (0..image.len()).step_by(width) {
        for i2 in (i..i + width).step_by(scale) {
            let mut sum = 0;
            for i3 in i2..i2 + scale {
                sum += image[i3];
            }
            result.push(sum / scale);
        }
    }
    result
}

fn generate() -> Vec<usize> {
    const COUNT: usize = 2_000_000;
    let image = [
        1, 0, 0, 4, 4, 0, 0, 1,
        0, 0, 0, 9, 9, 0, 0, 0,
        0, 0, 0, 9, 9, 0, 0, 0,
        4, 9, 9, 9, 9, 9, 9, 4,
        4, 9, 9, 9, 9, 9, 9, 4,
        0, 0, 0, 9, 9, 0, 0, 0,
        0, 0, 0, 9, 9, 0, 0, 0,
        1, 0, 0, 4, 4, 0, 0, 1,
    ];
    let mut result: Vec<usize> = Vec::with_capacity(COUNT * image.len());
    for _ in 0..COUNT {
        for j in 0..image.len() {
            result.push(image[j]);
        }
    }
    result
}

fn csum(thingy: &[usize]) -> usize {
    let mut s = 0;
    for i in 0..thingy.len() {
        s += thingy[i];
    }
    return s;
}

fn main() {
    let image = generate();
    let result1 = csum(&resize_image(&image, 8, 2));
    let result2 = csum(&resize_image(&image, 32, 8));
    let result3 = csum(&resize_image(&image, 16, 4));
    println!("{}, {}, {}", result1, result2, result3);
}
