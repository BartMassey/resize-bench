# Copyright © 2018 Benedikt Terhechte and Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file LICENSE in the source
# distribution of this software for license terms.

PROGS = bench-swift bench-rs bench-c bench-c-gcc

all: $(PROGS)

bench-swift: bench.swift
	swiftc -O -o bench-swift bench.swift

bench-rs: bench.rs
	rustc -C lto=fat -C opt-level=3 -o bench-rs bench.rs

bench-c: bench.c
	clang -Wall -O3 -o bench-c bench.c

bench-c-gcc: bench.c
	gcc -Wall -O3 -o bench-c-gcc bench.c
clean:
	-rm -f $(PROGS)
